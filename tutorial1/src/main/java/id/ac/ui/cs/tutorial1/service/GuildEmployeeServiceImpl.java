package id.ac.ui.cs.tutorial1.service;

import id.ac.ui.cs.tutorial1.model.GuildClerk;
import id.ac.ui.cs.tutorial1.model.GuildEmployee;
import id.ac.ui.cs.tutorial1.model.GuildMasterStaff;
import id.ac.ui.cs.tutorial1.repository.GuildEmployeeMemoryRepository;
import id.ac.ui.cs.tutorial1.repository.GuildEmployeeRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class GuildEmployeeServiceImpl implements GuildEmployeeService {
    GuildEmployeeRepository repository = new GuildEmployeeMemoryRepository();

    @Override
    public GuildEmployee addEmployee(String name, String familyName, String birthDate, String type) {

        GuildEmployee newGuildEmployee = null;
        Date birthDateConverted = convertStringToDate(birthDate);
        if (type.equalsIgnoreCase("Clerk")) {
            newGuildEmployee = new GuildClerk(name, familyName, generateId(), birthDateConverted);
        } else {
            newGuildEmployee = new GuildMasterStaff(name, familyName, generateId(), birthDateConverted);
        }
        repository.addEmployee(newGuildEmployee);
        return newGuildEmployee;
    }



    private String generateId() {
        return String.valueOf(repository.findAll().size());
    }

    @Override
    public GuildEmployee findById(String id) {
        return repository.findById(id);
    }

    @Override
    public List<GuildEmployee> findAll() {
        return repository.findAll();
    }

    private Date convertStringToDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException("Not supported Pattern");
        }
    }
}
